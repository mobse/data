Content Description  
====
Here you find the files for each of the **six runs** of [Giacobbo & Mapelli (2018)](https://ui.adsabs.harvard.edu/abs/2018MNRAS.480.2011G/abstract), *'The progenitors of compact-object binaries: impact of metallicity, common envelope and natal kicks'*.



Each folder corresponds to a model/run:  
A1 ---> alpha 1  
A3 ---> alpha 3  
A5 ---> alpha 5  

CC15A1 ---> CC15 alpha 1  
CC15A3 ---> CC15 alpha 3  
CC15A5 ---> CC15 alpha 5

In each folder there are **five type of files**:

1. the files named **data\_Y\_XX.txt**  
where Y = BHBs, BHNS, DNSs refers to the binary type and XX is the metallicity (absolute values, so solar metallicity is Z=0.02)  
Inside each file, you find the following information.  
First line of each file:   
**col. 1**: total mass of the simulated systems  
**col. 2**: number of systems merging in < Hubble time in the population-synthesis set in the file  
Following lines:  
**col. 1**: binary ID  
**col. 2**: zams mass of stellar progenitor 1 [Msun]  
**col. 3**: zams mass of stellar progenitor 2 [Msun]  
**col. 4**: mass of compact object 1 [Msun] NOTE that compact object 1 is not necessarily more massive than compact object 2, it is just the compact object that forms from stellar progenitor 1  
**col. 5**: mass of compact object 2 [Msun]  
**col. 6**: mass of compact object 1 + mass of compact object 2 [Msun]  
**col. 7**: delay time [Gyr]  
**col. 8**: semi-major axis [Rsun] when the binary becomes double degenerates  
**col. 9**: eccentricity when the binary becomes double degenerates  

2. Files with names like **table\_merger\_per\_unit\_mass.txt**  
each file has 4 columns with the following meaning  
**col. 1**: metallicity  
**col. 2**: number of double black holes per unit solar mass (as defined in Giacobbo & Mapelli 2018, eq. 8)  
**col. 3**: the same as col.2 but for black hole-neutron star binaries  
**col. 4**: the same as col.2 but for double neutron stars  


3. Files with names like **table\_number\_of\_mergers.txt**  
each file has 4 columns with the following meaning  
**col. 1**: metallicity  
**col. 2**: number of double black holes merging systems  
**col. 3**: the same as col.2 but for black hole-neutron star binaries  
**col. 4**: the same as col.2 but for double neutron stars  


4. Files with names like **table\_total\_mass.txt**  
each file has 3 columns with the following meaning  
**col. 1**: metallicity  
**col. 2**: total mass [Msun] of the evoleved stellar population  
**col. 3**: total numeber of simulated binaries  


5. **evolution.tar.gz** contains **CONTROL\_XX** where you can find an sintetic evolution of all the merging systems:  
**col. 1**: m1[Msun] at time *col. 5*  
**col. 2**: m2[Msun] at time *col. 5*  
**col. 3**: BSE stellar type star1  
**col. 4**: BSE stellar type star2  
**col. 5**: time [Gyr]  
**col. 6**: orbital separation [Rsun] at time *col. 5*  
**col. 7**: eccentricity at time *col. 5*  
**col. 8**: label describing what happens

