Data
===

In this repository we have been collected the data produced with [MOBSE](https://mobse-webpage.netlify.com/) and presented in our published papers.

**COB_2018** ---> *The progenitors of compact-object binaries: impact of metallicity, 
 common envelope and natal kicks"*
 [Giacobbo N. & Mapelli M., MNRAS 480, 2011–2030 (2018)](https://ui.adsabs.harvard.edu/abs/2018MNRAS.480.2011G/abstract)
